#!/usr/bin/env python3
from flask import Flask

app = Flask(__name__)

#“Routes” will handle all requests to a specific resource indicated in the
#@app.route() decorator

#Here we handle queries to the server directory
@app.route('/')
def index():
	return "Bonjour, le monde !"
#End declaration of server directory route

#will only execute if this file is run
if __name__ == "__main__":
	app.run(debug=True)
